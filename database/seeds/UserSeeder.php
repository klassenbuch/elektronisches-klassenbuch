<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 27.03.2017
 * Time: 16:30
 */

namespace database\seeds;


use App\models\Role;
use App\models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = Role::where('name', 'teacher')->first();

        $vey = new User();
        $vey->email = "vey@test.com";
        $vey->password = Hash::make('1234');
        $vey->vorname = 'Hubert';
        $vey->nachname = 'Vey';
        $vey->geschlecht = true;
        $vey->save();

//        $vey->roles()->attach($teacher->id);

        $traud = new User();
        $traud->email = "traud@test.com";
        $traud->password = Hash::make('1234');
        $traud->vorname = 'test';
        $traud->nachname = 'Traud';
        $traud->geschlecht = false;
        $traud->save();

//        $traud->roles()->attach($teacher->id);

        $reuter = new User();
        $reuter->email = "reuter@test.com";
        $reuter->password = Hash::make('1234');
        $reuter->vorname = 'Jörg';
        $reuter->nachname = 'Reuter';
        $reuter->geschlecht = true;
        $reuter->save();

  //      $reuter->roles()->attach($teacher->id);


        //schüler
        $student = Role::where('name', 'student')->first();

        $user = new User();
        $user->email = "daniel.brenzel@test.com";
        $user->password = Hash::make('1234');
        $user->vorname = 'Daniel';
        $user->nachname = 'Brenzel';
        $user->geschlecht = true;
        $user->save();
    //    $user->roles()->attach($student->id);

        $user = new User();
        $user->email = "baier@test.com";
        $user->password = Hash::make('1234');
        $user->vorname = 'Baier';
        $user->nachname = 'Michelle';
        $user->geschlecht = false;
        $user->save();
      //  $user->roles()->attach($student->id);

        $user = new User();
        $user->email = "pinguin@test.com";
        $user->password = Hash::make('1234');
        $user->vorname = 'Peter';
        $user->nachname = 'Pinguin';
        $user->geschlecht = false;
        $user->save();
        //$user->roles()->attach($student->id);
    }
}
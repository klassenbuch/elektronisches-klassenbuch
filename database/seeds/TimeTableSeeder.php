<?php

use App\models\Form;
use App\models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Collection;

class TimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher_ids = $this->getIdsAsArray(User::all());
        $form_ids = $this->getIdsAsArray(Form::all());
        foreach ($form_ids as $form_id) {
            for ($day = 1; $day <= 5; $day++) {

                for ($hour = 1; $hour <= 8; $hour++) {
                    shuffle($teacher_ids);
                    DB::table(CreateHourTable::tablename)->insert([
                        'weekday' => $day,
                        'room' => rand(1, 2) . "." . rand(1, 180),
                        'teacher_id' => $teacher_ids[0],
                        'hour' => $hour,
                        'Subject' => "Lernfeld " . rand(1, 12),
                        'form_id'=> $form_id
                    ]);
                }
            }
        }
    }

    /**
     * Parst aus einem Elequent Abrageergenis die ids
     * @param $resultSelect Collection eine Elequent abrage
     * @return array die Ids des Ergebnis
     */
    private function getIdsAsArray(Collection $resultSelect)
    {
        $results = [];
        foreach ($resultSelect as $result) {
            array_push($results, $result->id);
        }
        return $results;
    }
}

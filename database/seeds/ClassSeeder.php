<?php

use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //AEler
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '12',
            'type' => 'B',
            'description' => "392"
        ]);
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '11',
            'type' => 'B',
            'description' => "392"
        ]);
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '10',
            'type' => 'B',
            'description' => "392"
        ]);

        //FISIs
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '12',
            'type' => 'B',
            'description' => "390"
        ]);
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '11',
            'type' => 'B',
            'description' => "390"
        ]);
        DB::table(CreateClassTable::tablename)->insert([
            'year_from' => '2016',
            'year_to' => '2017',
            'class_year' => '10',
            'type' => 'B',
            'description' => "390"
        ]);
    }
}

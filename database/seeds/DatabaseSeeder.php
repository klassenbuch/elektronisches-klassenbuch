<?php

use database\seeds\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //no requirements
        $this->call(RoleSeeder::class);

        //requires other seeder
        $this->call(UserSeeder::class);
        $this->call(ClassSeeder::class);
        $this->call(TimeTableSeeder::class);

    }
}

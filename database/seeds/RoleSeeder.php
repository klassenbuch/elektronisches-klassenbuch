<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name'=> 'teacher',
            'display_name' => 'Lehrer',
        ]);
        DB::table('roles')->insert([
            'name'=> 'student',
            'display_name' => 'Schüler',
        ]);
    }
}

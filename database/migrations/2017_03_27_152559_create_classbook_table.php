<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassbookTable extends Migration
{

    const tablename = "classbook";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(self::tablename, function (Blueprint $table) {
            $table->increments('id')->index();
            $table->dateTime('erstelltAm');
            $table->dateTime('abgeschlossenAm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::tablename);
    }
}

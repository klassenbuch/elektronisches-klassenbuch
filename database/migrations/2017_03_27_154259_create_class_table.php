<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTable extends Migration
{
    const tablename = 'forms';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::tablename, function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('description');
            $table->integer('year_from');
            $table->integer('year_to');
            $table->integer('class_year');
            $table->string('type');
        });
        Schema::table(CreateHourTable::tablename, function ($table) {
            $table->integer('form_id')->unsigned();
            $table->foreign('form_id')->references('id')->on(self::tablename)->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CreateHourTable::tablename, function ($table) {
            $table->dropForeign('hour_form_id_foreign');
        });
        Schema::drop(self::tablename);

    }
}

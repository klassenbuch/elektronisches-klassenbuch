<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHourTable extends Migration
{

    const tablename = 'hour';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::tablename, function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('weekday')->nullable(false);
            $table->string('room'); //TODO eventuell auch durch RaumId ersetzen
            $table->integer('hour')->nullable();
            $table->integer('teacher_id')->unsigned();
            $table->string('subject'); //TODO durch FachId ersetzen

            $table->foreign('teacher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::tablename, function ($table) {
            $table->dropForeign('hour_teacher_id_foreign');
        });
        Schema::drop(self::tablename);

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserClass extends Migration
{
    const tablename = 'user_hour';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::tablename, function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('user_id')->unsigned();
            $table->integer('hour_id')->unsigned();

            $table->foreign('user_id')->references('id')->on(CreateUsersTable::tablename)->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('hour_id')->references('id')->on(CreateHourTable::tablename)->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::tablename, function ($table) {
            $table->dropForeign('user_hour_hour_id_foreign');
            $table->dropForeign('user_hour_user_id_foreign');
        });
        Schema::drop(self::tablename);

    }
}

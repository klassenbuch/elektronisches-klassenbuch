<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    const tablename = 'entries';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::tablename, function (Blueprint $table) {
            $table->increments('id');
            $table->string('short-comment');
            $table->longText('comment');
            $table->timestamps();

            $table->integer('creator')->unsigned();
            $table->integer('hour_id')->unsigned();
            $table->integer('classbook_id')->unsigned();

            $table->foreign('creator')->references('id')->on(CreateUsersTable::tablename)->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('hour_id')->references('id')->on(CreateHourTable::tablename)->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('classbook_id')->references('id')->on(CreateClassbookTable::tablename)->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::tablename, function ($table) {
            $table->dropForeign('departments_creator_foreign');
            $table->dropForeign('departments_hour_id_foreign');
        });
        Schema::dropIfExists(self::tablename);

    }
}

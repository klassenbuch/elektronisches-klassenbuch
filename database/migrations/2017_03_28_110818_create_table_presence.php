<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePresence extends Migration
{
    const tablename = 'presence';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::tablename, function (Blueprint $table) {
            $table->date('date');
            $table->string('reason');
            $table->integer('user_hour_id')->unsigned();

            $table->foreign('user_hour_id')->references('id')->on(CreateTableUserClass::tablename)->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::tablename, function ($table) {
            $table->dropForeign('presence_user_hour_id_foreign');
        });
        Schema::drop(self::tablename);

    }
}

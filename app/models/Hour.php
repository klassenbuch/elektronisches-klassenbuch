<?php

namespace App\models;

use CreateHourTable;
use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = CreateHourTable::tablename;

    public function teacher(){
        return $this->belongsTo('App\models\User');
    }
}
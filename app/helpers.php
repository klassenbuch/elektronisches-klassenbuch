<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 24.03.2017
 * Time: 17:34
 */

namespace App\models;

use Illuminate\Support\Facades\Auth;

class helpers
{
    public static function can($permission){
        return Auth::user()->can($permission);
    }
}
<?php

namespace App\Http\Controllers;

use App\models\Hour;
use Illuminate\Http\Request;

class TimetableController extends Controller
{
    public function show(Request $request, $class){


        $hours = [];
        for($count = 1; $count<= 8; $count++){

            $stunde = Hour::with('teacher')->where('form_id', $class)->where('hour', $count)->orderBy('weekday', 'asc')->get();
            array_push($hours, $stunde);
        }
        return view('stundenplan', ['hours'=>$hours]);
    }
}

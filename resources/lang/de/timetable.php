<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 23.03.2017
 * Time: 14:40
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'wochentag_mo' => 'Montag',
    'wochentag_di' => 'Dienstag',
    'wochentag_mi' => 'Mittwoch',
    'wochentag_do' => 'Donnerstag',
    'wochentag_fr' => 'Freitag',
    'wochentag_sa' => 'Samstag',
    'wochentag_so' => 'Sonntag',
];
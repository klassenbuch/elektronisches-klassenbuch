@extends('layouts.app')
@section('content')
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <tr class="row even">
                <td class="col"></td>
                <td class="col">@lang('timetable.wochentag_mo')</td>
                <td class="col">@lang('timetable.wochentag_di')</td>
                <td class="col">@lang('timetable.wochentag_mi')</td>
                <td class="col">@lang('timetable.wochentag_do')</td>
                <td class="col">@lang('timetable.wochentag_fr')</td>
            </tr>
            @foreach($hours as $hour)

                <tr class="row">


                    <td class="col">
                        {{ array_search($hour, $hours)+1 }}. Stunde<br/>
                    </td>

                    @foreach($hour as $cell)
                        <td class="col">{{ $cell->subject }}<br/>{{ $cell->teacher->nachname }}<br/>{{ $cell->room }}</td>
                    @endforeach
                </tr>
            @endforeach


        </table>
    </div>
@endsection
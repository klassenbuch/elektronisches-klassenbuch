<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Digitales Klassenbuch</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
            <li><a href="/stundenplan/1">Stundenplan</a></li>  {{-- TODO Link anpassen, sodass man immer zu dem eigenen Stundenplan kommt--}}
            <li><a href="#">Vertretungsplan</a></li>
            <li><a href="#">Noten</a></li>
            <li><a href="#">Lernfelder</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Verwalten</a></li>
            <li role="separator" class="divider"></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
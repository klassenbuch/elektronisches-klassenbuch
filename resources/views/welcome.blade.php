@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                Herzlich Willkommen im digitalen Stundenplan. Bitte loggen Sie sich <b>hier</b> ein.
            </div>
        </div>
    </div>
</div>
@endsection

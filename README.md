# README #

Ziel dieses Projektes ist ein digitales [Klassenbuch](https://bitbucket.org/klassenbuch/elektronisches-klassenbuch/wiki/browse/) für die Ferdinand-Braun-Schule Fulda. 


## Entwickler-Guide ##
* Bitte haltet Euch an den HTML5 Standard (kein xHTML oder HTML4).
* Benutzt keine (i)Frames.
* Erstellt bei umfangreichen Änderungen einen eigenen Branch.
* Alle Bezeichner von Klassen, Methoden usw. sind sprechend zu bennenen!!! (vor allem im Client)
* Alle HTML Formulare und Anfragen sollen [csrf](https://laravel.com/docs/5.4/routing)-Schutz enthalten
* So wenig Logik wie möglich im Frontend (Blade / HTML5)

## Versionsverwaltung##
Für die Versionsverwaltung dieses Projektes wird Git eingesetzt. Grundlegende Informationen darüber sind [hier](https://git-scm.com/book/de/v1/Git-Grundlagen) erhältlich. Eine Kurzanweisung für die Installation ist weiter unten zu finden

## Installationsanweisung ##

Vor den folgenden Anweisungen ist die ***Wächterkarte auszuschalten***!!!!!!

### Setezn der Umgebungsvariablen ###

Im Schulnetz gibt es einen Proxy. Dies ermöglicht es der Schule den kompletten HTTP-Verkehr mit zu schneiden. Um ins Internet zu kommen muss man Programmen sagen, dass sie über diesen Server gehen sollen. Gemäß dieses [Tutorials](https://www.nextofwindows.com/how-to-addedit-environment-variables-in-windows-7) muss dafür folgende Umgebungsvariablen gesetzt werden:

http_proxy  =  http://fbs:fbs@proxy.schulen-fulda.de:8080

### Installation von Programmen###

Installeiere alle Programme aus *K:\Klassenbuch\Installieren*

Die Standarteinstellungen können übernommen werden.

Die PATH-Variable sollte um den Pfad zur php.exe (C:\xampp\php\php.exe) erweitert werden, sodass der Pfad zu php nicht jedes mal komplett eingegeben werden muss.

### Clonen des Projektes ###

In der Git-Bash folgende Zeilen nacheinander ausführen und als letztes die *Clone*-Zeile aus bitbucket kopieren und ausführen.

```
#!bash

git config --global http.proxy http://fbs:fbs@proxy.schulen-fulda.de:8080
cd /W:

```

Nach dem der *Clone*-Befehl ausgeführt wurde, sollte im persönlichem Laufwerk ein Ordner "elektronisches-klassenbuch" angelegt worden sein. 

Hier findet ihr eine .env.example Datei. Diese ist ein Beispiel für die globale Konfiguration. Hier sollte man die Daten für die Datenbank eintragen. Default Einstellung sollte sein (kein Passwort):

* DB_DATABASE=klassenbuch
* DB_USERNAME=root
* DB_PASSWORD=


Nun muss noch die Datenbank gestartet werden. Hierzu starte das xampp control pannel. Klicke auf "Start" bei Apache und MySQL.

_Damit man im Webbroswer etws sieht müssen noch die Abhängigkeiten heruntergeladen, die Datenbank initialisiert, und den 
Um in **PHP Storm** die Befehlszeile zu öffnen drückt **Strg+Shift+X**. Anschließend folgende Zeilen ausführen:

```
#!cmd

composer install
php artisan migrate:refresh --seed
php artisan serve

```

Nach dem die .env.example nach .env (dies ist die globale Konfigurationsdatei die je nach Datenbank angepasst werden kann) kopiert wurde ist die Installation beendet.

Anschließend könnt ihr auf **[dieser Seite](http://localhost:8000/)** den aktuellen Stand ansehen. Der Reload von Änderungen erfolgt automatisch.

## Dokumentation ##
Die Dokumentationen liegt im Ordner documentation.